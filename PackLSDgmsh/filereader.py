#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2012 Jan Domurath <jan.domurath@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import division, with_statement, print_function

import numpy as np

def _remove_empty_lines_at_end(lines):
    """remove empty lines at the end of a file

    **Parameters:**

    - ``lines`` : list of lines
    """
    # well it turns out, there was a "bug" in my test, I shouldn't need this
    #
    # also, I think, modifying the original list in a loop would be better then
    # creating shallow copies
    if not lines[-1].strip():
        return _remove_empty_lines_at_end(lines[:-1])
    else:
        return lines


class LSDFileReader(object):
    """Reader for PackLSD output files

    **Parameters:**

    - ``fname`` : the file
    """
    def __init__(self, fname):
        self._fname = fname

        with open(self._fname, 'r') as f:
            self._lines = f.readlines()
            self._lines = _remove_empty_lines_at_end(self._lines)

        self._nDim = self._readDim()
        self._isMono = self._readFileType()
        self._nParticles, self._nSpecies = self._readParticlesAndSpecies()
        if self._isMono:
            self._nParticlesPerSpecies = self._readNParticlesPerSpecies()
        # self._normalVectors = self._readNormalVectors()
        self._hardWalls = self._readWallType()


    def _readDim(self):
        """read the dimension

        **returns:**

        - ``int`` : the dimension
        """
        return int(self._lines[0].strip().split()[0])

    def _readFileType(self):
        """read the type of the file

        ``True`` if it is as "mono"-disperse file else ``False``

        .. note::
           "mono" means, that there are sets of particles with distinct radii
           and **not** distributions, see the PackLSD documentation [PackLSDdoc]_

        **returns:**

        - ``bool`` : ``True`` if "mono"

        **raises:**

        - ``ValueError`` : if it can't identify the format

        .. references::

        .. [PackLSDdoc] PackLSD documentation, at `http://cherrypit.princeton.edu/donev/Packing/PackLSD/Instructions.html <http://cherrypit.princeton.edu/donev/Packing/PackLSD/Instructions.html>`_.
        """
        s = self._lines[0].strip().split()[2]
        if s == "mono":
            return True
        elif s == "poly":
            return False
        else:
            raise ValueError("This seems to be neither" \
                            + "a mono nor a poly disperse file!?")

    def _readParticlesAndSpecies(self):
        """read the number of particles and species

        **returns:**

        - ``tuple(int, int)`` : particles, species
        """
        nPnS = self._lines[1].strip().split()
        return tuple([int(i) for i in nPnS])

    def _readNParticlesPerSpecies(self):
        """read the number of particles per species

        **returns:**

        - ``tuple(int, int, ...)`` : particles per species #1, #2, ...

        **raises:**

        - ``NotImplementedError`` : if it is a "poly"-file

        .. todo::
           Check how this looks like for a "poly"-file and implement reader.

           The doc's say:

           "One for each specie (each specie is a distribution for polydisperse packings, rather than a fixed size)"

           whatever that means
        """
        if not self._isMono:
            raise NotImplementedError("Sorry! Haven't done that yet.")

        s = self._lines[2].strip().split()
        return tuple([int(i) for i in s])

    def _readNormalVectors(self):
        """read the normal vectors

        this isn't implemented yet

        .. todo::
           implement it
        """
        raise NotImplementedError("Sorry! Haven't done that yet.")

    def _readWallType(self):
        """read the type of Walls

        ``True`` : if it is a "hard" wall, else ``False``

        **returns:**

        - ``tuple(bool, bool [, bool])`` : "hard" ; x, y [, z]

        .. note::
           "hard" means particles can't intersect the wall
        """
        pos = 4 + self._isMono

        walls = self._lines[pos].strip().split()

        return tuple([ True if i == "F" else False for i in walls])

    @property
    def nDim(self):
        """Get the number of dimensions; ``2`` or ``3``
        """
        return self._nDim

    @property
    def nParticles(self):
        """Get the number of particles in the box (``int``)
        """
        return self._nParticles

    @property
    def nSpecies(self):
        """Get the number of species (``int``)
        """
        return self._nSpecies

    @property
    def hardWalls(self):
        """return the type of Walls

        ``True`` : if it is a "hard" wall, else ``False``

        **returns:**

        - ``tuple(bool, bool [, bool])`` : "hard"? ; x, y [, z]

        .. note::
           "hard" means particles can't intersect the wall
        """

        return self._hardWalls

    def getParticles(self):
        """returns an ndarray of the particles

        **returns:**

        - ``ndarry(x, y [,z], r)`` : center coordinates and radius
        """
        if self._isMono:
            return self._readMonoParticles()
        else:
            return self._readPolyParticles()

    def _readMonoParticles(self):
        """read particles for "mono" file
        """
        particles = np.zeros((self._nParticles, self._nDim + 1), dtype=np.float)

        # read the midpoints
        for idx, part in enumerate(self._lines[5 + self._isMono :]):
            particles[idx,0:self._nDim] = [float(i) for i in part.strip().split()]

        # set the radii
        radii = [float(i)/2. for i in self._lines[3].strip().split()]
        pos = 0
        for radius, num in zip(radii, self._nParticlesPerSpecies):
            particles[pos:pos+num,self._nDim] = np.ones(num) * radius
            pos += num

        return particles

    def _readPolyParticles(self):
        """read particles for "poly" file
        """
        particles = np.zeros((self._nParticles, self._nDim + 1), dtype=np.float)
        for idx, part in enumerate(self._lines[5 + self._isMono :]):
            particles[idx,:] = [float(i) for i in part.strip().split()]

        particles[:,self._nDim] = particles[:,self._nDim] / 2.

        return particles

if __name__ == '__main__':
    import os.path
    fnames = ["PackLSD_bidisperse.dat", ]
    basepath = os.path.dirname(__file__)
    filepath = os.path.abspath(os.path.join(basepath, "..", "data", fnames[0]))

    reader = LSDFileReader(filepath)
    # print(reader.nDim)
    # print(reader._isMono)
    # print(reader.nParticles)
    # print(reader.nSpecies)
    # print(reader._readWallType())

    print(reader.getParticles())
