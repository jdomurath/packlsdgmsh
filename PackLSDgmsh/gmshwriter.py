#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2012 Jan Domurath <jan.domurath@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import division, with_statement, print_function

from string import Template
from math import pi
from textwrap import dedent

from filereader import LSDFileReader

class GmshFileCreator(object):
    """A creator for gmsh ".geo" files

    **Parameters:**

    - ``template`` : the name of the template file
    """

    def __init__(self, template):
        self._template = template
        self._callbacks  = []

    def addCallback(self, callback):
        """add a callback

        A callback has to return too strings, the template key and its
        expansion.

        A callback is a function, that takes **no** arguments.

        **Parameters:**

        - ``callback`` : callable or ``list`` of callables
        """
        if isinstance(callback, list):
            self._callbacks += callback
        else:
            self._callbacks.append(callback)

    def makeFile(self):
        """generate the gmsh file

        **returns:**

        - ``string`` : the file
        """
        with open(self._template, 'r') as theTpl:
            tplString = theTpl.read()

        keys = {}
        for callback in self._callbacks:
            key, string = callback()
            keys[key] = string

        theFile = Template(tplString)

        return theFile.substitute(keys)



def _key(i):
    """a helper function, so that we have different outer scopes in closures

    also see comment in PackLSDgmsh._get_boxSize()
    """
    return lambda : i


class PackLSDgmsh(object):
    """Create a gmsh file for a given PackLSD file.

    **Parameters:**

    - ``fname`` : the filename of the PackLSD output
    - ``template`` : the template to use, if ``None`` (the default) a default
       template will be used
    """

    def __init__(self, fname, template=None):
        self._fname = fname
        self._template = template

        self._hloc = None
        self._lcar = 0.1     # this value is arbitrary
        self._approx_elem_hole = None
        self._elem_order = None

        self._reader = LSDFileReader(self._fname)

        if self._template is None:
            import os.path
            tplname = "PackLSDgmsh.geo.tpl"
            basepath = os.path.dirname(__file__)
            self._template = os.path.abspath(os.path.join(basepath, "..",
                                                   "tpl", tplname))
        self._gmshCreator = GmshFileCreator(self._template)

    @property
    def hloc(self):
        """hloc of the mesh

        this is the approximate size of an element at the boundary of the box
        """
        return self._hloc

    @hloc.setter
    def hloc(self, val):
        self._hloc = val

    @property
    def lcar(self):
        """lcar of the mesh

        this is the approximate size of an element at the boundary of a
        particle

        .. note::
           This value defaults to 0.1 **AND** it is arbitrary, it will be
           overridden by the :meth:`estimate_meshSizeAtHole` method
        """
        return self._lcar

    @lcar.setter
    def lcar(self, val):
        self._lcar = val

    @property
    def nElementsHole(self):
        """approx. number of elements at the boundary of each particle
        """
        return self._approx_elem_hole

    @nElementsHole.setter
    def nElementsHole(self, val):
        self._approx_elem_hole = val

    @property
    def elementOrder(self):
        """order of the elements (<6)
        """
        return self._elem_order

    @elementOrder.setter
    def elementOrder(self, val):
        self._elem_order = val

    def _get_boxSize(self):
        """generate the size functions for the box size

        **returns:**

        - ``list`` of functions : callbacks for :class:`GmshFileCreator`
        """
        sizes = [("size_x_min", "0"),
                 ("size_y_min", "0"),
                 ("size_z_min", "0"),
                 ("size_x_max", "1"),
                 ("size_y_max", "1"),
                 ("size_z_max", "1")]
        ret = []
        for i in sizes:
            # ret.append(lambda : i)
            # this doesn't work, because the outer scope for all the `lambdas'
            # is the same
            #
            # in this case all the functions in the `ret' list will return
            # `("size_z_max", "1")' because this is the last `i' defined in
            # the loop (the outer scope)

            # hence use a separate function, this way each has it's own outer
            # scope
            ret.append(_key(i))
        return ret

    def _get_hloc(self):
        """get the callback for hloc

        **returns:**

        - ``function`` : callback for :class:`GmshFileCreator`

        **raises:**

        - ``ValueError`` : if :attr:`hloc` isn't set
        """
        if self._hloc is not None:
            return _key(("hloc", "%f" % self._hloc))
        else:
            raise ValueError("please set hloc")

    def _get_lcar(self):
        """get the callback for lcar

        **returns:**

        - ``function`` : callback for :class:`GmshFileCreator`
        """
        return lambda : ("lcar", "%f" % self._lcar)

    def _get_elementOrder(self):
        """get the callback for element order

        **returns:**

        - ``function`` : callback for :class:`GmshFileCreator`

        **raises:**

        - ``ValueError`` : if :attr:`elementOrder` isn't set
        """
        if self._elem_order is not None:
            return lambda : ("ElementOrder", "%d" % self._elem_order)
        else:
            raise ValueError("please set elementOrder")

    def estimate_meshSizeAtHole(self, r):
        """estimate the mesh size for a hole

        **Parameters:**

        - ``r`` : the radius of the hole

        **returns:**

        - ``tuple`` : size at hole boundary, size at ``dist`` from hole, ``dist``

        **raises:**

        - ``ValueError`` : if :attr:`elementOrder` isn't set

        .. warning::
           I don't really know what I'm doing here, this is just the simplest
           version I could come up with.

           You might want to subclass this one.

        .. note::
           How it works:

           The minimum size :math:`l_{min}` is given by:

           .. math::
              l_{min} = \dfrac{0.5 \pi r}{n_e}

           with :math:`r` the radius of the particle and :math:`n_e` the
           approx. number of elements (:attr:`nElementsHole`)


           The maximum size :math:`l_{max}` is equal to hloc (:attr:`hloc`):

           .. math::
              l_{max} = hloc

           The distance :math:`d` between which the element size is linearly
           interpolated is:

           .. math::
              d = 2 r
        """
        if self._approx_elem_hole is None:
            raise ValueError("please set nElementsHole")

        arc_lenth = 0.5 * pi * r  # length of quarter circle arc
        holeLcMin = arc_lenth / self._approx_elem_hole
        holeDistMax = 2 * r
        holeLcMax = self._hloc # 2 * holeLcMin * (1 + holeDistMax)

        return holeLcMin, holeLcMax, holeDistMax

    def _makeOneHole(self, t, x, y, z, r):
        """generate the template string for one sphere

        **Parameters:**

        - ``t`` : the number of the sphere
        - ``x`` : the x coordinate of the center
        - ``y`` : the y coordinate of the center
        - ``z`` : the z coordinate of the center
        - ``r`` : the r radius

        **returns:**

        - ``string`` : template string
        """
        sphr_str = """\
        t = ${t};
        x = ${x} ; y = ${y} ; z = ${z} ; r = ${r};
        holeLcMin = ${holeLcMin};
        holeLcMax = ${holeLcMax};
        holeDistMax = ${holeDistMax};
        Call CheeseHole;
        """
        holeLcMin, holeLcMax, holeDistMax = self.estimate_meshSizeAtHole(r)

        tpl = Template(dedent(sphr_str))

        keys = {"t" : "%d" % t,
                "x" : "%f" % x,
                "y" : "%f" % y,
                "z" : "%f" % z,
                "r" : "%f" % r,
                "holeLcMin" : "%f" % holeLcMin,
                "holeLcMax" : "%f" % holeLcMax,
                "holeDistMax" : "%f" % holeDistMax,}
        return tpl.substitute(keys)

    def _makeHoles(self):
        """get the callback for the holes

        **returns:**

        - ``function`` : callback for :class:`GmshFileCreator`
        """
        hole_strings = []
        for idx, hole in enumerate(self._reader.getParticles()):
            hole_strings.append(self._makeOneHole(idx+1, *hole))

        return lambda : ("theHoles", "\n\n".join(hole_strings))

    def makeGmshFile(self):
        """generate the gmsh file

        **returns:**

        - ``string`` : the file
        """
        self._gmshCreator.addCallback(self._get_boxSize())
        self._gmshCreator.addCallback(self._get_hloc())
        self._gmshCreator.addCallback(self._get_lcar())
        self._gmshCreator.addCallback(self._get_elementOrder())
        self._gmshCreator.addCallback(self._makeHoles())

        return self._gmshCreator.makeFile()
