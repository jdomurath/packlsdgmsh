Function CheeseHole
   // this function is adopted from the gmsh tutorial (t5.geo)

   p1 = newp; Point(p1) = {x,  y,  z,  lcar} ;
   p2 = newp; Point(p2) = {x+r,y,  z,  lcar} ;
   p3 = newp; Point(p3) = {x,  y+r,z,  lcar} ;
   p4 = newp; Point(p4) = {x,  y,  z+r,lcar} ;
   p5 = newp; Point(p5) = {x-r,y,  z,  lcar} ;
   p6 = newp; Point(p6) = {x,  y-r,z,  lcar} ;
   p7 = newp; Point(p7) = {x,  y,  z-r,lcar} ;

   c1 = newreg; Circle(c1) = {p2,p1,p7};
   c2 = newreg; Circle(c2) = {p7,p1,p5};
   c3 = newreg; Circle(c3) = {p5,p1,p4};
   c4 = newreg; Circle(c4) = {p4,p1,p2};
   c5 = newreg; Circle(c5) = {p2,p1,p3};
   c6 = newreg; Circle(c6) = {p3,p1,p5};
   c7 = newreg; Circle(c7) = {p5,p1,p6};
   c8 = newreg; Circle(c8) = {p6,p1,p2};
   c9 = newreg; Circle(c9) = {p7,p1,p3};
   c10 = newreg; Circle(c10) = {p3,p1,p4};
   c11 = newreg; Circle(c11) = {p4,p1,p6};
   c12 = newreg; Circle(c12) = {p6,p1,p7};

   l1 = newreg; Line Loop(l1) = {c5,c10,c4};   Ruled Surface(newreg) = {l1};
   l2 = newreg; Line Loop(l2) = {c9,-c5,c1};   Ruled Surface(newreg) = {l2};
   l3 = newreg; Line Loop(l3) = {c12,-c8,-c1}; Ruled Surface(newreg) = {l3};
   l4 = newreg; Line Loop(l4) = {c8,-c4,c11};  Ruled Surface(newreg) = {l4};
   l5 = newreg; Line Loop(l5) = {-c10,c6,c3};  Ruled Surface(newreg) = {l5};
   l6 = newreg; Line Loop(l6) = {-c11,-c3,c7}; Ruled Surface(newreg) = {l6};
   l7 = newreg; Line Loop(l7) = {-c2,-c7,-c12};Ruled Surface(newreg) = {l7};
   l8 = newreg; Line Loop(l8) = {-c6,-c9,c2};  Ruled Surface(newreg) = {l8};


   theloops[t] = newreg ;

   f1 = fieldcounter;
   
   Field[f1] = Attractor;
   Field[f1].NodesList = {p1,p2,p3,p4,p5,p6,p7};
   Field[f1].EdgesList = {c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12};
   Field[f1].FacesList = {l8+1,l5+1,l1+1,l2+1,l3+1,l7+1,l6+1,l4+1};
   fieldcounter = fieldcounter+1;
   f2 = fieldcounter;
   
   Field[f2] = Threshold;
   Field[f2].IField = f1;
   Field[f2].LcMin = holeLcMin;
   Field[f2].LcMax = holeLcMax;
   Field[f2].DistMin = 0.;
   Field[f2].DistMax = holeDistMax;
   minList[f2] = f2;
   fieldcounter = fieldcounter+1;

   Surface Loop(theloops[t]) = {l8+1,l5+1,l1+1,l2+1,l3+1,l7+1,l6+1,l4+1};

   //// needs gmsh >= 2.6
   // Field[f1] = BoundaryLayer;
   // // Field[f1].NodesList = {p1,p2,p3,p4,p5,p6,p7};
   // // Field[f1].EdgesList = {c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12};
   // // Field[f1].FacesList = {l8+1,l5+1,l1+1,l2+1,l3+1,l7+1,l6+1,l4+1};
   // Field[f1].FacesList = {theloops[t]};
   // Field[f1].Quads = 0;
   // Field[f1].hfar = 0.1;       // element size far from wall
   // Field[f1].hwall_n = 0.01;
   // Field[f1].hwall_t = 0.01;
   // Field[f1].thickness = 0.1;
   // Field[f1].ratio = 1.01;
   // minList[f1] = f1;
   // fieldcounter = fieldcounter+1;

   thehole = newreg ;
   theHoles[t] = thehole;
   Volume(thehole) = theloops[t] ;

Return

/*---------------------------------------------------------------------------*/
// basic setup

// size of the box
size_x_min = ${size_x_min};
size_y_min = ${size_y_min};
size_z_min = ${size_z_min};

size_x_max = ${size_x_max};
size_y_max = ${size_y_max};
size_z_max = ${size_z_max};


hloc = ${hloc};  // approx. element size at the boundary of the box


lcar = ${lcar};  // approx. element size at the boundary of the particles
                 // this value is somewhat useless, this will later be 
                 // overwritten with `holeLcMin'; just set it to some value
                 // (greater than 0)

/*---------------------------------------------------------------------------*/
// make the box

Point(1) = {size_x_min, size_y_min, size_z_min, hloc};
Point(2) = {size_x_max, size_y_min, size_z_min, hloc};
Point(3) = {size_x_max, size_y_max, size_z_min, hloc};
Point(4) = {size_x_min, size_y_max, size_z_min, hloc};

Point(5) = {size_x_min, size_y_min, size_z_max, hloc};
Point(6) = {size_x_max, size_y_min, size_z_max, hloc};
Point(7) = {size_x_max, size_y_max, size_z_max, hloc};
Point(8) = {size_x_min, size_y_max, size_z_max, hloc};

Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};

Line(5) = {5,6};
Line(6) = {6,7};
Line(7) = {7,8};
Line(8) = {8,5};

Line(9)  = {1,5};
Line(10) = {2,6};
Line(11) = {3,7};
Line(12) = {4,8};

// bottom
Line Loop(21) = {-1,-4,-3,-2};
Plane Surface(31) = {21} ;

// top
Line Loop(22) = {5,6,7,8};
Plane Surface(32) = {22} ;

// left
Line Loop(23) = {1,10,-5,-9};
Plane Surface(33) = {23} ;

// right
Line Loop(24) = {12,-7,-11,3};
Plane Surface(34) = {24} ;

// front
Line Loop(25) = {2,11,-6,-10};
Plane Surface(35) = {25} ;

// back
Line Loop(26) = {9,-8,-12,4};
Plane Surface(36) = {26} ;

/*---------------------------------------------------------------------------*/
// make the holes

fieldcounter = 1;

minList[] = {};     // list to keep track of the MinFields
theHoles[] = {};    // list of "holes" aka particles

// and now we make all the holes in the cheese

// the code should look like:

/******************************************************************************

t = 1   // the number of the hole, starts at 1
x = 0. ; y = 0. ; z = 0. ; r = 0.2 ;  // center and radius
holeLcMin = 0.1;    // the approx. element size at the hole boundary
holeLcMax = 0.5;    // the approx. element size at `holeDistMax'
holeDistMax = 0.3;  // ... the element size is linearly interpolated form 
                    // ... `holeLcMin' to `holeLcMax' in between `holeDistMax'

Call CheeseHole ;   // make the hole

******************************************************************************/

${theHoles}


/*---------------------------------------------------------------------------*/
// put it together

Physical Volume (1) = {theHoles[]};

minField = fieldcounter;
Field[minField] = Min;
Field[minField].FieldsList = {minList[]};

Background Field = minField;

theloops[0] = newreg ;

Surface Loop(theloops[0]) = {31,32,33,34,35,36};

matrixV = newreg;
Volume(matrixV) = {theloops[]};


Physical Volume (0) = matrixV ;

Mesh.CharacteristicLengthExtendFromBoundary = 0;  // refine in both directions 
                                                  // from the boundary

Mesh.ElementOrder = ${ElementOrder};  // order of the elements (<6)