The gmshwriter module
=====================

This module creates the gmsh file.

-----------------
Available classes
-----------------
.. currentmodule:: PackLSDgmsh.gmshwriter

.. autosummary::
   :toctree: reference/

   GmshFileCreator
   PackLSDgmsh
