The filereader module
=====================

This module contains a reader for the PackLSD dat files.


-----------------
Available classes
-----------------
.. currentmodule:: PackLSDgmsh.filereader

.. autosummary::
   :toctree: reference/

   LSDFileReader
