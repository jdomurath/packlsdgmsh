Introduction
============

This code is merely a proof of concept, so be warned things will break.


What it can and can't
---------------------

* it can only handle "hard" walls, meaning the particles can't intersect the
  walls

  to get "hard" walls in PackLSD the variable ``lb_periodic`` in the input file
  should have the value ``F F F``

* it only works in 3D
* it can't compute the exact volume of the particles from the mesh
* the unit vectors (``lb_lattice_vectors`` in PackLSD) must be "standard"
  Cartesian, meaning

  .. math::

     e_{x}=\left(\begin{array}{c}
                                  1\\
                                  0\\
                                  0
                 \end{array}\right)
     \qquad
     e_{y}=\left(\begin{array}{c}
                                  0\\
                                  1\\
                                  0
                 \end{array}\right)
     \qquad
     e_{z}=\left(\begin{array}{c}
                                  0\\
                                  0\\
                                  1
                 \end{array}\right)

* some things aren't implemented yet, you will sometimes get a
  ``NotImplementedError``


How it works
------------

Basically it generates a gmsh ".geo" file from a template.

You can find one in ``tpl/PackLSDgmsh.geo.tpl``.


The ``packLSDgmsh`` program
---------------------------

This program is just an example on how to use the code.

Use it like:

.. code-block:: bash

   packLSDgmsh [options] PackFile.dat

A simple input file can be found at: ``test/files/test_file.dat``.

For the options:

``hloc``
  This is the approximate element size at the boundary of the box

``nelem``
  This is the approx. number of elements a the boundary of a particle.

  Or more exact: The approx. number of elements along a quarter of the
  perimeter of a circle with the same radius as the particle.

  OK: did someone understand that??
