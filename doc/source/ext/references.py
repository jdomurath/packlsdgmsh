from docutils import nodes

from sphinx.util.compat import Directive

def setup(app):
    app.add_node(references,
                 html=(html_visit_ref, html_depart_ref),
                 latex=(latex_visit_ref, latex_depart_ref))

    app.add_directive('references', ReferencesDirective)


# class references(nodes.Titular, nodes.TextElement):
#     pass

class references(nodes.rubric):  # same as above
    pass


class ReferencesDirective(Directive):

    def run(self):
        return [references()]


def html_visit_ref(self, node):
    self.body.append(u'<p class="rubric">References</p>')


def html_depart_ref(self, node):
    pass


def latex_visit_ref(self, node):
    pass


def latex_depart_ref(self, node):
    pass
