Welcome to PackLSDgmsh documentation!
=====================================

.. toctree::
   :maxdepth: 2

   intro.rst
   reference.rst

Indices and tables
==================
* :ref:`genindex`
* :ref:`search`

