#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2012 Jan Domurath <jan.domurath@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import division, with_statement

from import_trick import * # is there a better way than this


from PackLSDgmsh.gmshwriter import GmshFileCreator, PackLSDgmsh




TEST_FILE_1 = """${id1}

${id2}
${id3}
"""
TEST_FILE_1_OUT = """123

456
789
"""

class test_GmshFileCreator_01(TestCase):

    @classmethod
    def setupClass(self):
        mock_open_function(TEST_FILE_1)
        self.writer = GmshFileCreator("bla")

        id1 = lambda : ("id1", "123")
        id2 = lambda : ("id2", "456")
        id3 = lambda : ("id3", "789")

        self.writer.addCallback(id1)
        self.writer.addCallback([id2, id3])

    @classmethod
    def tearDownClass(self):
        reset_open()

    def test_makeFile(self):
        self.assertEqual(self.writer.makeFile(), TEST_FILE_1_OUT)


TEST_FILE_2 = """${id1}
${id2}
${id3}
${id4}
${id5}
${id6}
${id7}
${id8}
${id9}
"""
TEST_FILE_2_OUT = """1
2
3
4
5
6
7
8
9
"""

def key(i):
   return lambda : ("id%d" %i, "%d" %i)

def genKeys(x):
    ret = []
    for i in x:
        # ret.append( lambda : ("id%d" %i, "%d" %i) ) # why doesn't this work??
        #
        # this doesn't work, because the outer scope for all the `lambdas'
        # is the same; the lambdas will always return the last `i' of the loop

        # hence use a separate function, this way each has it's own outer
        # scope
        ret.append(key(i))
    return ret

class test_GmshFileCreator_02(TestCase):

    @classmethod
    def setupClass(self):
        mock_open_function(TEST_FILE_2)
        self.writer = GmshFileCreator("bla")

        self.writer.addCallback(genKeys(range(1, 10)))

    @classmethod
    def tearDownClass(self):
        reset_open()

    def test_makeFile(self):
        self.assertEqual(self.writer.makeFile(), TEST_FILE_2_OUT)


class test_PackLSDgmsh(TestCase):

    @classmethod
    def setupClass(self):
        self.pack = PackLSDgmsh('test/files/test_file.dat')

    def test_hloc(self):
        with self.assertRaises(ValueError):
            self.pack._get_hloc()

    def test_elementOrder(self):
        with self.assertRaises(ValueError):
            self.pack._get_elementOrder()

    def test_elementOrder2(self):
        with self.assertRaises(ValueError):
            self.pack.estimate_meshSizeAtHole(0.2)

