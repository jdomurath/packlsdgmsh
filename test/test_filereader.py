#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2012 Jan Domurath <jan.domurath@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import division, with_statement

from import_trick import *  # is there a better way than this



from PackLSDgmsh.filereader import LSDFileReader


TEST_FILE_1 = """           3 HS mono
         2           2
          1          1
   0.4        0.6     
    1.0000000000000000         0.0000000000000000         0.0000000000000000         0.0000000000000000         1.0000000000000000         0.0000000000000000         0.0000000000000000         0.0000000000000000         1.0000000000000000     
 F F F
   0.1        0.2        0.3     
   0.4        0.5        0.6  

   
"""


class test_LSDFileReader_mono(TestCase):

    @classmethod
    def setupClass(self):
        mock_open_function(TEST_FILE_1)
        self.reader = LSDFileReader("bla")
        reset_open()

    def test_nDim(self):
        self.assertEqual(self.reader.nDim, 3)

    def test_nParticles(self):
        self.assertEqual(self.reader.nParticles, 2)

    def test_nSpecies(self):
        self.assertEqual(self.reader.nSpecies, 2)

    def test_hardWalls(self):
        self.assertEqual(self.reader.hardWalls, (True, True, True))

    def test_isMono(self):
        self.assertTrue(self.reader._isMono)

    def test_getParticles(self):
        res = np.array([[0.1, 0.2, 0.3, 0.2],
                        [0.4, 0.5, 0.6, 0.3]])
        npt.assert_allclose(self.reader.getParticles(), res)


TEST_FILE_1_POLY = """3 HS poly
2  1
2
1.0    0.0   0.0   0.0   1.0   0.0   0.0   0.0   1.0
F F F
   0.1        0.2        0.3     0.4
   0.4        0.5        0.6     0.6

   
"""


class test_LSDFileReader_poly(TestCase):

    @classmethod
    def setupClass(self):
        mock_open_function(TEST_FILE_1_POLY)
        self.reader = LSDFileReader("bla")
        reset_open()

    def test_nDim(self):
        self.assertEqual(self.reader.nDim, 3)

    def test_nParticles(self):
        self.assertEqual(self.reader.nParticles, 2)

    def test_nSpecies(self):
        self.assertEqual(self.reader.nSpecies, 1)

    def test_hardWalls(self):
        self.assertEqual(self.reader.hardWalls, (True, True, True))

    def test_isMono(self):
        self.assertFalse(self.reader._isMono)

    def test_getParticles(self):
        res = np.array([[0.1, 0.2, 0.3, 0.2],
                        [0.4, 0.5, 0.6, 0.3]])
        npt.assert_allclose(self.reader.getParticles(), res)
