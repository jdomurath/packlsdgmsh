#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2012 Jan Domurath <jan.domurath@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


from __future__ import division, with_statement

import __builtin__
from contextlib import closing
import StringIO
import sys

import numpy as np
import numpy.testing as npt

from distutils.version import StrictVersion


if sys.version_info[0] == 2 and  sys.version_info[1] < 7:
    from unittest2 import TestCase
else:
    from unittest import TestCase


# check numpy, assert_allclose was new in 1.5?
if StrictVersion(np.__version__) < StrictVersion('1.6'):
    npt.assert_allclose = npt.assert_array_almost_equal


# mock open in with
ORIG_OPEN = __builtin__.open


def mock_open_function(ftext):
    def open(*args, **kwargs):
        return closing(StringIO.StringIO(ftext))

    __builtin__.open = open

def reset_open():
    __builtin__.open = ORIG_OPEN
