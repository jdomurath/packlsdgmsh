SHELL:=/bin/bash -O extglob

.PHONY: help doc-html doc-pdf runtests clean dist-clean

CWD=${CURDIR}

help:
	@echo "Please use \`make <target>' where <target> is one of"
	@echo "  doc-html     to make the docs as html"
	@echo "  doc-pdf      to make the docs as pdf"
	@echo "  runtests     to run the test suite"
	@echo "  clean        to remove bytecompiled files and *~ files"
	@echo "  dist-clean   to remove the docs, setup files and clean"

doc-html:
	cd doc && make html

doc-pdf:
	cd doc && make latexpdf

runtests:
	PYTHONPATH=${CWD}:${PYTHONPATH} nosetests

clean:
	find -iname "*.pyc" | xargs rm -f
	find -iname "*~" | xargs rm -f

dist-clean: clean
	cd doc && make clean
	cd doc/source && rm -rf reference/
	rm -rf dist/ *.egg-info/ build/ cover/
